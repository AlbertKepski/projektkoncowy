<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('UTF-8');


if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {

    $redirect = '/phpjs1zab/blert/projektkoncowy/Projekt#con';

    $temat= "=?UTF-8?B?".base64_encode("Wiadomość systemowa")."?=";
    $errors = [];

    if (empty($_POST['name'])) {
        $errors[] = 'Proszę podać imię';
    }

    if (empty($_POST['surname'])) {
        $errors[] = 'Proszę podać nazwisko';
    }

    if (empty($_POST['surname']) || strlen($_POST['number']) > 9 || strlen($_POST['number']) < 9) {
        $errors[] = 'Proszę podać  dziewięcio cyfrowy numer';
//        };
    }

    if (empty($_POST['email'])) {
        $errors[] = 'Proszę podać email';
    }

    if (empty($_POST['subject'])) {
        $errors[] = 'Brak treści wiadomości';
    }


    $content = 'Imię wysyłającego: '. $_POST['name'] .PHP_EOL. '<br/>' .
                'Nazwisko: ' . $_POST['surname'].PHP_EOL. '<br/>'.
                'Email: ' . $_POST['email'].PHP_EOL.'<br/>'.
                'Treść wiadomości: ' . $_POST['subject'];
    $mail = 'albertkepski3@gmail.com';

    if (!empty($errors)) {
        $_SESSION['errors'] = $errors;
    } else {
        mail($mail, $temat, $content,
            'Content-Type: text/html; charset=UTF-8');

        $_SESSION['success'] = true;
    }

    header('location: '.$redirect);
    exit;
}

require __DIR__.'/template/main.phtml';

$_SESSION['success'] = false;
$_SESSION['errors'] = [];