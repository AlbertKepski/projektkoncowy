$(function() {
   $("form[name='registration']").validate({

       rules: {
           name: {required: true, minlength: 3},
           surname: "required",
           number: "required",
           email: {
               required: true,
               email: true
           }
       },
       messages: {
           name: "Proszę wpisać imię",
           surname: "Proszę wpisać nazwisko",
           number: "Proszę wpisać numer",
           email: "Proszę wpisać email"
       },

        submitHandler: function (form) {
            form.submit();
        }

   });



});