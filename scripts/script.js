$(document).ready(function(){

    // scroll menu

    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing');
    });

    // slide toggle phone icon

    $('.phone_icon').click(function(){
        $(".slide_icon").slideToggle("slow");
    });
});





