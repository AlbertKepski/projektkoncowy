
<?php
session_start();

if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
    $errors = [];
    
    if (empty($_POST['imie'])) {
        $errors[] = 'Prosze podac imie';
    }
    
    if (!empty($errors)) {
        $_SESSION['errors'] = $errors;
        
        header('location: /');
        exit;
    }
    
    mail('alert@kepski.pl', 'Tyt', 'wiadaomosc');
    
    $_SESSION['success'] = true;
    header('location: /');
    exit;
}
?>


<?php if (!empty($_SESSION['success'])): ?>
<div class="success">DZIEKUJEMY ZA KONTAKT. POSTARAM SIE ODPOWIEDZIEC JAK TO TYLKO BEDZIE MOZLIWE</div>
<?php else: ?>
<?php if (!empty($_SESSION['errors'])): foreach($_SESSION['errors'] as $error): ?>
<div class="error"><?= $error ?></div>
<?php endforeach; endif ?>
<form>
</form>
<?php endif ?>



//    $redirect = sprintf(
//        'http://%s%s',
//        $_SERVER['HTTP_HOST'],
//        $_SERVER['REQUEST_URI']
//    );
